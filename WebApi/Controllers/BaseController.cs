﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi.Controllers
{
    /// <summary>
    /// https://restfulapi.net/http-methods/#summary
    /// </summary>
    public class BaseController : ApiController
    {
        protected IHttpActionResult ResponseOk<T>(T obj)
        {
            return Content<T>(HttpStatusCode.OK, obj);
        }

        protected IHttpActionResult ResponseCreated<T>(T obj)
        {
            return Content<T>(HttpStatusCode.Created, obj);
        }

        protected IHttpActionResult ResponseNotFound<T>(T obj)
        {
            return Content<T>(HttpStatusCode.NotFound, obj);
        }
    }
}
