﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Business;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class MessageController : BaseController
    {
        private MessageService messageService;

        public MessageController()
        {
            messageService = new MessageService();
        }

        public IHttpActionResult Post(Message message)
        {
            var messageOk = messageService.Create(message);
            if (messageOk.HasError)
            {
                return ResponseNotFound<Message>(messageOk);
            }
            return ResponseCreated<Message>(messageOk);
        }

        public IHttpActionResult Get()
        {
            var list = messageService.GetAll();
            if (list.ToList().Count < 1)
            {
                var notFound = new ResponseModel("", "Nenhum registro foi encontrado");
                return ResponseNotFound<ResponseModel>(notFound);
            }
            return ResponseOk<List<Message>>(list.ToList());
        }

        [Route("api/Message/{id}")]
        public IHttpActionResult Get(int id)
        {
            var obj = messageService.GetById(id);
            return (obj.HasError) ? ResponseNotFound<Message>(obj) : ResponseOk<Message>(obj);
        }

        [Route("api/Message/GetTest")]
        public IHttpActionResult GetTest()
        {
            var obj = messageService.GetById(1);
            return (obj.HasError) ? ResponseNotFound<Message>(obj) : ResponseOk<Message>(obj);
        }

        /// <summary>
        /// https://gist.github.com/tugberkugurlu/7318417
        /// </summary>
        [Route("api/Message/{id}")]
        public IHttpActionResult Patch(long id, Message message)
        {
            var obj = messageService.PatchText(id, message.Text);
            return (obj.HasError) ? ResponseNotFound<Message>(obj) : ResponseOk<Message>(obj);
        }

        public IHttpActionResult Put(Message message)
        {
            var obj = messageService.Put(message);
            return (obj.HasError) ? ResponseNotFound<Message>(obj) : ResponseOk<Message>(obj);
        }

        [Route("api/Message/Delete/{id}")]
        public IHttpActionResult Delete(long id)
        {
            var obj = messageService.Delete(id);
            return (obj.HasError) ? ResponseNotFound<Message>(obj) : ResponseOk<Message>(obj);
        }

        #region Seed
        // Postman json input
        /*{
	        "Id":1,
            "Text":"ana",
            "Access":1
        }*/

        //output
        /*{
            "Text": "",
            "Access": 1,
            "Date": "0001-01-01T00:00:00",
            "Id": 1,
            "HasError": true,
            "ListResponseModel": [
                {
                    "Field": "Text",
                    "Text": "Texto Obrigatório",
                    "Type": 0
                }
            ]
        }*/
        #endregion
    }
}
