﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Models;

namespace WebApi.Business
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/aspnet/mvc/overview/older-versions-1/models-data/validating-with-a-service-layer-cs
    /// </summary>
    public class MessageService
    {

        #region functions private
        private static List<Message> messages = new List<Message>();

        private void ValidateText(Message message)
        {
            if (string.IsNullOrEmpty(message.Text))
                message.AddResponseModel("Text", "Campo Texto é obrigatório");
        }

        private void ValidateCreate(Message message)
        {
            ValidateText(message);

            if (message.Access < 1)
                message.AddResponseModel("Access", "Campo Acesso não está definido");
        }

        private void ValidateEdit(Message message)
        {
            if (message.Id < 1)
                message.AddResponseModel("Id", "Campo Id não está definido");

            ValidateText(message);

            if (message.Access < 1)
                message.AddResponseModel("Access", "Campo Acesso não esta definido");
        }
        #endregion

        public Message Create(Message message)
        {
            ValidateCreate(message);

            if (!message.HasError)
            {
                var id = messages.Count + 1;
                message.Id = id;
                message.Date = DateTime.Now;
                messages.Add(message);
            }

            return message;
        }

        public IQueryable<Message> GetAll()
        {
            return messages.AsQueryable();
        }

        public Message GetById(long id)
        {
            var obj = messages.Where(x => x.Id == id).FirstOrDefault();
            if (obj == null)
            {
                obj = new Message(id);
                obj.HasError = true;
                var notFound = new ResponseModel("", "Registro não foi encontrado");
                obj.ListResponseModel.Add(notFound);
                return obj;
            }

            return obj;
        }

        public Message PatchText(long id, string text)
        {
            var obj = GetById(id);
            if (obj.HasError)
            {
                return obj;
            }

            obj.Text = text;
            ValidateText(obj);
            return obj;
        }

        public Message Put(Message message)
        {
            ValidateEdit(message);
            if (message.HasError)
            {
                return message;
            }

            var obj = GetById(message.Id);
            if (obj.HasError)
            {
                return obj;
            }

            int position = messages.IndexOf(obj);
            messages.RemoveAt(position);
            messages.Add(obj);

            return obj;
        }

        public Message Delete(long id)
        {
            var obj = GetById(id);
            if (obj.HasError)
            {
                return obj;
            }

            int position = messages.IndexOf(obj);
            messages.RemoveAt(position);

            var responseModel = 
                new ResponseModel("Registro removido com sucesso", (short)ResponseModel.ResponseTypeModel.Info);
            obj.ListResponseModel.Add(responseModel);

            return obj;
        }
    }
}