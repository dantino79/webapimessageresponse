﻿using System.Collections.Generic;

namespace WebApi.Models
{
    public class ResponseValidModel
    {
        public long Id { get; set; }
        public bool HasError { get; set; } = false;
        public List<ResponseModel> ListResponseModel { get; set; } = new List<ResponseModel>();

        public void AddResponseModel(string field, string text)
        {
            ResponseModel obj = new ResponseModel(field, text);
            ListResponseModel.Add(obj);
            HasError = true;
        }
    }
}