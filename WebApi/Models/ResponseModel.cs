﻿namespace WebApi.Models
{
    public class ResponseModel
    {
        public enum ResponseTypeModel : short
        {
            Error = 0,
            Warning = 1,
            Info = 2
        };

        public ResponseModel(string field, string text)
        {
            this.Field = field;
            this.Text = text;
            this.Type = (short)ResponseTypeModel.Error;
        }

        public ResponseModel(string text, short type)
        {
            this.Text = text;
            this.Type = type;
        }

        public string Field { get; }
        public string Text { get; }
        public short Type { get; }
    }
}