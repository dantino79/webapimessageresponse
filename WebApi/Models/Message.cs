﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class Message : ResponseValidModel
    {
        public string Text { get; set; }
        public int Access { get; set; }
        public DateTime Date { get; set; }

        public Message(long id)
        {
            this.Id = id;
        }
    }
}